package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;

public class AreaCientifica implements DataValidation {

    private Long id;
    private String nome;

    private List<Curso> cursos = new ArrayList<>();

    public void adicionarCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    @Override
    public void validateForDataModification() {
        if (StringUtils.isBlank(this.nome) || this.nome.length() > 50) {
            throw new SeminariosCientificosException("ER0010");
        }
        if (this.cursos == null) {
            throw new ObjetoNuloException();
        }
        for (Curso curso : this.cursos) {
            if (curso == null) {
                throw new ObjetoNuloException();
            }
        }
    }

}
