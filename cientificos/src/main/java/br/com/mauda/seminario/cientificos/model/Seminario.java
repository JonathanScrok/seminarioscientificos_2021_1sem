package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;

public class Seminario implements DataValidation {

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private LocalDate data;
    private Integer qtdInscricoes;

    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdeInscricoes) {
        this.areasCientificas.add(areaCientifica);
        this.professores.add(professor);
        professor.adicionarSeminario(this);
        this.qtdInscricoes = qtdeInscricoes;
        for (int i = 0; i < qtdeInscricoes; i++) {
            new Inscricao(this);
        }
    }

    public void adicionarProfessor(Professor professor) {
        this.professores.add(professor);
    }

    public void adicionarAreaCientifica(AreaCientifica area) {
        this.areasCientificas.add(area);
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public boolean possuiAreaCientifica(AreaCientifica area) {
        return this.areasCientificas.contains(area);
    }

    public boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);
    }

    public Long getId() {
        return this.id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public LocalDate getData() {
        return this.data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    @Override
    public void validateForDataModification() {
        if (this.data == null || this.data.isBefore(LocalDate.now())) {
            throw new SeminariosCientificosException("ER0070");
        }
        if (StringUtils.isBlank(this.descricao) || this.descricao.length() > 200) {
            throw new SeminariosCientificosException("ER0071");
        }
        if (StringUtils.isBlank(this.titulo) || this.titulo.length() > 50) {
            throw new SeminariosCientificosException("ER0072");
        }
        if (this.mesaRedonda == null) {
            throw new SeminariosCientificosException("ER0073");
        }
        if (this.qtdInscricoes == null || this.qtdInscricoes <= 0) {
            throw new SeminariosCientificosException("ER0074");
        }

        this.validarAreaCientifica();
        this.validarProfessores();

        if (this.inscricoes == null) {
            throw new ObjetoNuloException();
        }

        for (Inscricao inscricao : this.inscricoes) {
            if (inscricao == null) {
                throw new ObjetoNuloException();
            }
            inscricao.validateForDataModification();
        }
    }

    private void validarAreaCientifica() {
        if (this.areasCientificas == null || this.areasCientificas.isEmpty()) {
            throw new SeminariosCientificosException("ER0076");

        }
        for (AreaCientifica areaCientifica : this.areasCientificas) {
            if (areaCientifica == null) {
                throw new ObjetoNuloException();
            }
            areaCientifica.validateForDataModification();
        }
    }

    private void validarProfessores() {
        if (this.professores == null || this.professores.isEmpty()) {
            throw new SeminariosCientificosException("ER0075");
        }
        for (Professor professor : this.professores) {
            if (professor == null) {
                throw new ObjetoNuloException();
            }
            professor.validateForDataModification();
        }
    }

}