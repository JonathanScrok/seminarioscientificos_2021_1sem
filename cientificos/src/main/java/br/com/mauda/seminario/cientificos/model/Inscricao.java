package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;

public class Inscricao implements DataValidation {

    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao = LocalDateTime.now();
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;

    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;
    private Estudante estudante;
    private Seminario seminario;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;

        seminario.adicionarInscricao(this);
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.estudante.adicionarInscricao(this);
        this.direitoMaterial = direitoMaterial;
        this.dataCompra = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.COMPRADO;

    }

    public void cancelarCompra() {
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.dataCompra = null;

    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
        this.dataCheckIn = LocalDateTime.now();

    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Long getId() {
        return this.id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    @Override
    public void validateForDataModification() {

        if (this.situacao == null) {
            throw new ObjetoNuloException();
        }
        if (this.seminario == null) {
            throw new ObjetoNuloException();
        }

        if (!SituacaoInscricaoEnum.DISPONIVEL.equals(this.situacao)) {
            if (this.direitoMaterial == null) {
                throw new ObjetoNuloException();
            }
            if (this.estudante == null) {
                throw new ObjetoNuloException();
            }
            this.estudante.validateForDataModification();
        }
    }

}
