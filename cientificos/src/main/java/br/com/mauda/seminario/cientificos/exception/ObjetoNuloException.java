package br.com.mauda.seminario.cientificos.exception;

public class ObjetoNuloException extends SeminariosCientificosException {

    private static final long serialVersionUID = -7140018396677613599L;

    public ObjetoNuloException() {
        super("ER0003");
    }
}
