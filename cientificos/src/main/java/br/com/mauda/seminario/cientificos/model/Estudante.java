package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;
import br.com.mauda.seminario.cientificos.util.EmailUtils;

public class Estudante implements DataValidation {

    private Long id;
    private String nome;
    private String telefone;
    private String email;

    private Instituicao instituicao;

    public Estudante(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    private List<Inscricao> inscricoes = new ArrayList<>();

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public void removerInscricao(Inscricao inscricao) {
        this.inscricoes.remove(inscricao);
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    // teste
    @Override
    public void validateForDataModification() {
        if (StringUtils.isBlank(this.email) || this.email.length() > 50) {
            throw new SeminariosCientificosException("ER0030");
        }
        if (!this.email.matches(EmailUtils.EMAIL_PATTERN)) {
            throw new SeminariosCientificosException("ER0030");
        }
        if (StringUtils.isBlank(this.nome) || this.nome.length() > 50) {
            throw new SeminariosCientificosException("ER0031");
        }
        if (StringUtils.isBlank(this.telefone) || this.telefone.length() > 15) {
            throw new SeminariosCientificosException("ER0032");
        }
        if (this.instituicao == null) {
            throw new ObjetoNuloException();
        }
        if (this.inscricoes == null) {
            throw new ObjetoNuloException();
        }
        for (Inscricao inscricao : this.inscricoes) {
            if (inscricao == null) {
                throw new ObjetoNuloException();
            }
        }
        this.instituicao.validateForDataModification();

    }
}
