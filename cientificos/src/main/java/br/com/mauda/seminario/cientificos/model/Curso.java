package br.com.mauda.seminario.cientificos.model;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;

public class Curso implements DataValidation {

    private Long id;
    private String nome;

    private AreaCientifica areaCientifica;

    public Curso(AreaCientifica areaCientifica) {
        this.areaCientifica = areaCientifica;
        areaCientifica.adicionarCurso(this);
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String name) {
        this.nome = name;
    }

    public AreaCientifica getAreaCientifica() {
        return this.areaCientifica;
    }

    @Override
    public void validateForDataModification() {
        if (StringUtils.isBlank(this.nome) || this.nome.length() > 50) {
            throw new SeminariosCientificosException("ER0020");
        }
        if (this.areaCientifica == null) {
            throw new ObjetoNuloException();
        }
        this.areaCientifica.validateForDataModification();
    }
}
